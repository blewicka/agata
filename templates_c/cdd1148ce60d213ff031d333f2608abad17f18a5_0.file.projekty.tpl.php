<?php
/* Smarty version 3.1.30, created on 2017-10-26 10:04:14
  from "/var/www/lighttpd/Agata/templates/projekty.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59f196fe357951_07063833',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cdd1148ce60d213ff031d333f2608abad17f18a5' => 
    array (
      0 => '/var/www/lighttpd/Agata/templates/projekty.tpl',
      1 => 1509005052,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59f196fe357951_07063833 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/var/www/lighttpd/Agata/smarty-3.1.30/libs/plugins/modifier.replace.php';
?>
<div id="projects-page">
    <div class="row justify-content-center">
        <h2><?php echo $_smarty_tpl->tpl_vars['projects']->value;?>
</h2>
    </div>
    </br>
    <div class="container">
        <div class="row">
            
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['projects_table']->value, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value) {
?>
                <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content='<?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['id']->value['zawartosc'],'new_project',$_smarty_tpl->tpl_vars['id']->value['id']);?>
' id="<?php echo $_smarty_tpl->tpl_vars['id']->value['id'];?>
">
                    
                    <img class="project-child" src="project_pics/<?php echo $_smarty_tpl->tpl_vars['id']->value['id'];?>
.jpg">
                </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


        </div>
    </div>
</div><?php }
}
