<?php
/* Smarty version 3.1.30, created on 2017-10-25 16:25:08
  from "/var/www/lighttpd/Agata/templates/index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59f09ec4d498d3_21331312',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '82c8476fa213e3b199600f6cc2943a0d773637bf' => 
    array (
      0 => '/var/www/lighttpd/Agata/templates/index.tpl',
      1 => 1508941490,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
    'file:witaj.tpl' => 1,
    'file:projekty.tpl' => 1,
    'file:o-mnie.tpl' => 1,
    'file:kontakt.tpl' => 1,
    'file:media.tpl' => 1,
  ),
),false)) {
function content_59f09ec4d498d3_21331312 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_104033407059f09ec4d26718_98421434', 'nav');
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_199382747759f09ec4d2d973_39926622', 'css');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_38428131259f09ec4d42635_89185934', 'body');
?>
 

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19232647559f09ec4d47f62_81321796', 'js');
?>

<?php $_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'nav'} */
class Block_104033407059f09ec4d26718_98421434 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#projects-page"> <?php echo $_smarty_tpl->tpl_vars['portfolio']->value;?>
 <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#about-me-page"><?php echo $_smarty_tpl->tpl_vars['about']->value;?>
<span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#contact-page"><?php echo $_smarty_tpl->tpl_vars['contact']->value;?>
 <span class="sr-only">(current)</span></a>
    </li>
    <div class="lang-buttons nav-item nav-link">
       <a href="?lang=pl">
                <img class="lang-button" alt="PL" src="icons2/pl-flag.png">
        </a>
           
        <a href="?lang=en">
            <img class="lang-button" alt="EN" src="icons2/en-flag.png" width="22" height="14">
        </a>
    </div>
<?php
}
}
/* {/block 'nav'} */
/* {block 'css'} */
class Block_199382747759f09ec4d2d973_39926622 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" type="text/css" href="css/witaj.css">
    <link rel="stylesheet" type="text/css" href="css/projekty.css">
    <link rel="stylesheet" type="text/css" href="css/o-mnie.css">
    <link rel="stylesheet" type="text/css" href="css/kontakt.css">
    <link rel="stylesheet" type="text/css" href="css/media.css">
    <link rel="stylesheet" type="text/css" href="css/overlay.css">

    <link rel="stylesheet" type="text/css" href="quill/quill.core.css">
<?php
}
}
/* {/block 'css'} */
/* {block 'body'} */
class Block_38428131259f09ec4d42635_89185934 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


    <div id="overlay" class="overlay">

    <!-- Button to close the overlay navigation -->
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

    <!-- Overlay content -->
    <div class="overlay-arrow left">
        &lsaquo;
    </div>

    <div class="overlay-arrow right">
        &rsaquo;
    </div>
    <div class="overlay-content">

    </div>

    </div> 

    <div class="page" id="welcome-page">

        <?php $_smarty_tpl->_subTemplateRender("file:witaj.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    </div>

    <div class="page" id="projects-page">

        <?php $_smarty_tpl->_subTemplateRender("file:projekty.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    </div>

    <div class="page" id="about-me-page">

        <?php $_smarty_tpl->_subTemplateRender("file:o-mnie.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    </div>

    <div class="page" id="contact-page">

        <?php $_smarty_tpl->_subTemplateRender("file:kontakt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    </div>

    <div class="page" id="media-page">

        <?php $_smarty_tpl->_subTemplateRender("file:media.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    </div>

    <div id="footer"/>

<?php
}
}
/* {/block 'body'} */
/* {block 'js'} */
class Block_19232647559f09ec4d47f62_81321796 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
   
   <?php echo '<script'; ?>
 src="js/projekty.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 src="js/nawigacja.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 src="jquery/js/jquery.easing.min.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'js'} */
}
