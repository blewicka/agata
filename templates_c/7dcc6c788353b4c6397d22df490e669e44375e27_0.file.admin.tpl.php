<?php
/* Smarty version 3.1.30, created on 2017-10-17 09:50:13
  from "/var/www/lighttpd/Agata/templates/admin.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59e5b635982d99_92003997',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7dcc6c788353b4c6397d22df490e669e44375e27' => 
    array (
      0 => '/var/www/lighttpd/Agata/templates/admin.tpl',
      1 => 1508226501,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
    'file:witaj-admin.tpl' => 1,
    'file:projekty-admin.tpl' => 1,
    'file:o-mnie-admin.tpl' => 1,
    'file:kontakt.tpl' => 1,
    'file:media-admin.tpl' => 1,
  ),
),false)) {
function content_59e5b635982d99_92003997 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_205126582159e5b63594ae78_82933655', 'nav');
?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_79334821059e5b635951b03_35047204', 'css');
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_134368110359e5b63597a0c2_36061009', 'body');
?>
 

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_99019931159e5b635980cb4_03787455', 'js');
?>

<?php $_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'nav'} */
class Block_205126582159e5b63594ae78_82933655 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#projects-page"> <?php echo $_smarty_tpl->tpl_vars['portfolio']->value;?>
 <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#about-me-page"><?php echo $_smarty_tpl->tpl_vars['about']->value;?>
<span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#contact-page"><?php echo $_smarty_tpl->tpl_vars['contact']->value;?>
 <span class="sr-only">(current)</span></a>
    </li>
    <div class="btn-group nav-item" data-toggle="buttons">
        <label class="btn btn-secondary active">
            <input type="radio" class="lang" data-lang="pl" checked autocomplete="off" id="btn_pl"> PL
        </label>
        <label class="btn btn-secondary">
            <input type="radio" class="lang" data-lang="en" autocomplete="off" id="btn_en"> EN
        </label>
    </div>
<?php
}
}
/* {/block 'nav'} */
/* {block 'css'} */
class Block_79334821059e5b635951b03_35047204 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <link rel="stylesheet" type="text/css" href="css/witaj.css">
    <link rel="stylesheet" type="text/css" href="css/projekty.css">
    <link rel="stylesheet" type="text/css" href="css/o-mnie.css">
    <link rel="stylesheet" type="text/css" href="css/kontakt.css">
    <link rel="stylesheet" type="text/css" href="css/media.css">
    <link rel="stylesheet" type="text/css" href="css/overlay.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css">
    <link rel="stylesheet" type="text/css" href="css/admin.css">

    <link rel="stylesheet" type="text/css" href="highlight/default.min.css">
    <link rel="stylesheet" type="text/css" href="katex/katex.min.css">
    <link rel="stylesheet" type="text/css" href="quill/quill.core.css">
    <link rel="stylesheet" type="text/css" href="quill/quill.snow.css">
<?php
}
}
/* {/block 'css'} */
/* {block 'body'} */
class Block_134368110359e5b63597a0c2_36061009 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['trans_resultpl']->value, 'trans');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['trans']->value) {
?>
        <div class="trans_result pl <?php echo $_smarty_tpl->tpl_vars['trans']->value['nazwa'];?>
 hidden">
            <?php echo $_smarty_tpl->tpl_vars['trans']->value['tekst'];?>

        </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['trans_resulten']->value, 'trans');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['trans']->value) {
?>
        <div class="trans_result en <?php echo $_smarty_tpl->tpl_vars['trans']->value['nazwa'];?>
 hidden">
            <?php echo $_smarty_tpl->tpl_vars['trans']->value['tekst'];?>

        </div>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>



    <div id="overlay" class="overlay">

    <!-- Button to close the overlay navigation -->
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

    <!-- Overlay content -->
    <div class="overlay-content">
        <form action="" onsubmit="return false">
            
            
            <label class="custom-file">
                <input type="file" id="project-pic" class="custom-file-input">
                <span class="custom-file-control"></span>
            </label>
            <br>
            <br>
            <br>
                <div id="project-editor">
                </div>
            
            <br>
            <button class="btn btn-primary" id="project-save">ZAPISZ</button></br></br>
            <button class="btn btn-primary" id="project-delete">USUŃ</button>

        </form>
    </div>
    </div>
    <div class="overlay fade hidden" id="loading">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
    <!-- Overlay content -->
    <div class="page" id="welcome-page">

        <?php $_smarty_tpl->_subTemplateRender("file:witaj-admin.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

        
    </div>

    <div class="page" id="projects-page">

        <?php $_smarty_tpl->_subTemplateRender("file:projekty-admin.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    </div>

    <div class="page" id="about-me-page">

        <?php $_smarty_tpl->_subTemplateRender("file:o-mnie-admin.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    </div>

    <div class="page" id="contact-page">

        <?php $_smarty_tpl->_subTemplateRender("file:kontakt.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    </div>

    <div class="page" id="media-page">

        <?php $_smarty_tpl->_subTemplateRender("file:media-admin.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


    </div>

    <div id="footer"/>

<?php
}
}
/* {/block 'body'} */
/* {block 'js'} */
class Block_99019931159e5b635980cb4_03787455 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>
   
   <?php echo '<script'; ?>
 src="js/nawigacja.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 src="jquery/js/jquery.easing.min.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 src="highlight/highlight.min.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 src="katex/katex.min.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 src="quill/quill.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 src="quill/quill.min.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 src="js/witaj-admin.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 src="js/omnie-admin.js"><?php echo '</script'; ?>
>
   <?php echo '<script'; ?>
 src="js/projekty-admin.js"><?php echo '</script'; ?>
> 
   <?php echo '<script'; ?>
 src="js/jezyk.js"><?php echo '</script'; ?>
> 
   <?php echo '<script'; ?>
 src="js/media-admin.js"><?php echo '</script'; ?>
> 
<?php
}
}
/* {/block 'js'} */
}
