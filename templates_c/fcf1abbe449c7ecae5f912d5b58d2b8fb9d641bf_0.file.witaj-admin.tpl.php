<?php
/* Smarty version 3.1.30, created on 2017-10-17 09:50:13
  from "/var/www/lighttpd/Agata/templates/witaj-admin.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59e5b6359960b4_92657352',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fcf1abbe449c7ecae5f912d5b58d2b8fb9d641bf' => 
    array (
      0 => '/var/www/lighttpd/Agata/templates/witaj-admin.tpl',
      1 => 1508226494,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e5b6359960b4_92657352 (Smarty_Internal_Template $_smarty_tpl) {
?>
 <div id="center-witaj">
  <div class="row justify-content-center">
    <div class="col-lg-6 col-md-8 col-xs-10">  
      <form action="" onsubmit="return false">
            <label class="custom-file">
                <input type="file" id="main-pic" class="custom-file-input">
                <span class="custom-file-control"></span>
            </label><br><br>

            <div class="form-group row">
              <div class="col-10">
                <input class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['welcome_title']->value;?>
" type="text" placeholder="Tytuł" id="welcome-title">
              </div>
            </div>

            <div class="form-group">
                
                <div id="welcome-editor">
                </div>
            </div>

            <button class="btn btn-info" id="welcome-save">ZAPISZ</button>

        </form>
    </div>
  </div>
</div><?php }
}
