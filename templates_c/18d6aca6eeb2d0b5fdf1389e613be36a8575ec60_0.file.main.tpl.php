<?php
/* Smarty version 3.1.30, created on 2017-10-23 15:01:30
  from "/var/www/lighttpd/Agata/templates/main.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59ede82a38a0b1_09017151',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '18d6aca6eeb2d0b5fdf1389e613be36a8575ec60' => 
    array (
      0 => '/var/www/lighttpd/Agata/templates/main.tpl',
      1 => 1508763682,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59ede82a38a0b1_09017151 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="BLewicka">

    <link rel="icon" type="image/png" href="icons2/favicon.png">
    <link rel="shortcut icon" href="icons2/favicon.png" type="image/png"/>

    
    <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link href='http://fonts.googleapis.com/css?family=Arima+Madurai&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_55928797459ede82a37ab56_35567607', 'css');
?>


  </head>

      <nav id="mainNav" class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-brand"><?php echo $_smarty_tpl->tpl_vars['head_title']->value;?>
</div>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="nav navbar-nav mr-auto">
        
        </ul>
        <ul class="nav navbar-nav ml-auto">
          <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_69924782659ede82a37fc32_92022271', 'nav');
?>

        </ul>
      </div>
    </nav>

    <div class="container-fluid" id="main">

     <div class="starter-template">

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_111777734459ede82a3828e4_10337126', 'body');
?>
 
    
     </div>
      
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
      <?php echo '<script'; ?>
 src="jquery/js/jquery-3.2.1.min.js"><?php echo '</script'; ?>
>
	  <?php echo '<script'; ?>
 src="popper/umd/popper.min.js"><?php echo '</script'; ?>
>
	  <?php echo '<script'; ?>
 src="bootstrap/js/bootstrap.min.js"><?php echo '</script'; ?>
>
    

   <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_53138005859ede82a385d21_44920798', 'js');
?>
 
  </body>
</html>
<?php }
/* {block 'css'} */
class Block_55928797459ede82a37ab56_35567607 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php
}
}
/* {/block 'css'} */
/* {block 'nav'} */
class Block_69924782659ede82a37fc32_92022271 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <?php
}
}
/* {/block 'nav'} */
/* {block 'body'} */
class Block_111777734459ede82a3828e4_10337126 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'body'} */
/* {block 'js'} */
class Block_53138005859ede82a385d21_44920798 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

   <?php
}
}
/* {/block 'js'} */
}
