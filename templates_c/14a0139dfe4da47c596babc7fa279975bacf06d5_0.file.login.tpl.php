<?php
/* Smarty version 3.1.30, created on 2017-10-19 14:31:24
  from "/var/www/lighttpd/Agata/templates/login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59e89b1c23b906_38834733',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '14a0139dfe4da47c596babc7fa279975bacf06d5' => 
    array (
      0 => '/var/www/lighttpd/Agata/templates/login.tpl',
      1 => 1508416279,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:main.tpl' => 1,
  ),
),false)) {
function content_59e89b1c23b906_38834733 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_54461183459e89b1c235448_14234315', 'body');
$_smarty_tpl->inheritance->endChild();
$_smarty_tpl->_subTemplateRender("file:main.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 2, false);
}
/* {block 'body'} */
class Block_54461183459e89b1c235448_14234315 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="row justify-content-center">
 <div class="col-lg-3 col-md-8 col-xs-12 ">

      <form class="form-signin" method="post" action="?page=admin&action=login">
       
        <h2 class="form-signin-heading">Please sign in</h2>
       
        <input type="text" id="login" name="login" class="form-control" placeholder="Login" required autofocus>

        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
       
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
</div>
<?php
}
}
/* {/block 'body'} */
}
