<?php
/* Smarty version 3.1.30, created on 2017-10-18 10:40:33
  from "/var/www/lighttpd/Agata/templates/kontakt.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59e71381875550_63422526',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '660e7d4bf323aa1205801a752f0dced14960c4ee' => 
    array (
      0 => '/var/www/lighttpd/Agata/templates/kontakt.tpl',
      1 => 1508316030,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e71381875550_63422526 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="center-contact">
    <div class="row justify-content-center">
        <div class="col-lg-5 col-md-8 col-xs-12 ">
                <h2><?php echo $_smarty_tpl->tpl_vars['contact']->value;?>
</h2>

                <form action="?action=send-mail" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="<?php echo $_smarty_tpl->tpl_vars['contact_name']->value;?>
" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" name="email" placeholder="<?php echo $_smarty_tpl->tpl_vars['contact_email']->value;?>
" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="<?php echo $_smarty_tpl->tpl_vars['contact_phone']->value;?>
" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="content" rows="5" name="content" placeholder="<?php echo $_smarty_tpl->tpl_vars['contact_message']->value;?>
" required></textarea>
                    </div>
                    
                    <button type="submit" name="submit" value="submit" class="btn btn-info"><?php echo $_smarty_tpl->tpl_vars['contact_submit']->value;?>
</button>
                </form>
            </div>
    </div>
</div><?php }
}
