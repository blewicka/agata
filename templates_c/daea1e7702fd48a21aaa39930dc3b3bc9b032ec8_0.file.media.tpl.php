<?php
/* Smarty version 3.1.30, created on 2017-10-19 13:51:20
  from "/var/www/lighttpd/Agata/templates/media.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59e891b8c30b68_61849424',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'daea1e7702fd48a21aaa39930dc3b3bc9b032ec8' => 
    array (
      0 => '/var/www/lighttpd/Agata/templates/media.tpl',
      1 => 1508398765,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e891b8c30b68_61849424 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="row justify-content-center">
       <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">
        <div class="col mini-headers"><?php echo $_smarty_tpl->tpl_vars['info']->value;?>
</div> 
        <div class="col bottom_text"><?php echo $_smarty_tpl->tpl_vars['info_content']->value;?>
</div> 
       </div>

       <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">
            <div class="col mini-headers"><?php echo $_smarty_tpl->tpl_vars['social_media']->value;?>
</div> 
            <a target="_blank" class="smedia-links" href="https://www.etsy.com/shop/ArtworkByAgata?ref=seller-platform-mcnav">
                <img  alt="Etsy" src="icons2/etsy-icon.png" width="40" height="40">
            </a>
           
            <a target="_blank" class="smedia-links" href="https://www.instagram.com/agata.handzel/">
                <img  alt="Instragram" src="icons2/insta-icon.png" width="40" height="40">
            </a>
            
            <a target="_blank" class="smedia-links" href="https://agatahandzel.deviantart.com/">
                <img  alt="DeviantArt" src="icons2/devart-icon.png" width="40" height="40">
            </a>  
             
       </div> 

        <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">
            <div class="col mini-headers"><?php echo $_smarty_tpl->tpl_vars['about']->value;?>
</div> 
            <div class="col bottom_text"><?php echo $_smarty_tpl->tpl_vars['about_short_content']->value;?>
</div> 
        </div>
</div>

<?php }
}
