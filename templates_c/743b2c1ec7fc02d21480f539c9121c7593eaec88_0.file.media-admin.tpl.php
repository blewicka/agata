<?php
/* Smarty version 3.1.30, created on 2017-10-19 14:43:50
  from "/var/www/lighttpd/Agata/templates/media-admin.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59e89e06b35347_26384548',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '743b2c1ec7fc02d21480f539c9121c7593eaec88' => 
    array (
      0 => '/var/www/lighttpd/Agata/templates/media-admin.tpl',
      1 => 1508398747,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e89e06b35347_26384548 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="row justify-content-center">
       <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">

            <div class="form-group">
                <textarea class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['info_content']->value;?>
" rows="3" placeholder="Info"  id="info-content"></textarea>
                
            </div>
       </div>

       <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">
            <div class="col mini-headers"><?php echo $_smarty_tpl->tpl_vars['social_media']->value;?>
</div> 
            <a target="_blank" class="smedia-links" href="https://www.etsy.com/shop/ArtworkByAgata?ref=seller-platform-mcnav">
                <img  alt="Etsy" src="icons2/etsy-icon.png" width="40" height="40">
            </a>
           
            <a target="_blank" class="smedia-links" href="https://www.instagram.com/agata.handzel/">
                <img  alt="Instragram" src="icons2/insta-icon.png" width="40" height="40">
            </a>
            
            <a target="_blank" class="smedia-links" href="https://agatahandzel.deviantart.com/">
                <img  alt="DeviantArt" src="icons2/devart-icon.png" width="40" height="40">
            </a>  
             
       </div> 

       <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">

            <div class="form-group">
                <textarea class="form-control" value="<?php echo $_smarty_tpl->tpl_vars['about_short_content']->value;?>
" rows="3" placeholder="O mnie"  id="about_short_content"></textarea>
                
            </div>

            <button class="btn btn-info" id="media-save">ZAPISZ</button>

       </div>
</div>

<?php }
}
