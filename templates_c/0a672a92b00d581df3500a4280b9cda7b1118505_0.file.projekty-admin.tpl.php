<?php
/* Smarty version 3.1.30, created on 2017-10-26 10:19:52
  from "/var/www/lighttpd/Agata/templates/projekty-admin.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.30',
  'unifunc' => 'content_59f19aa8de8d39_45355816',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0a672a92b00d581df3500a4280b9cda7b1118505' => 
    array (
      0 => '/var/www/lighttpd/Agata/templates/projekty-admin.tpl',
      1 => 1509005269,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59f19aa8de8d39_45355816 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_replace')) require_once '/var/www/lighttpd/Agata/smarty-3.1.30/libs/plugins/modifier.replace.php';
?>
<div id="projects-page">
    <div class="row justify-content-center">
        <h2><?php echo $_smarty_tpl->tpl_vars['projects']->value;?>
</h2>
    </div>
    </br>
    <div class="container">
        <div class="row" id="projects-container">
            

            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['proj_contentpl']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
                <div class="proj_content pl <?php echo $_smarty_tpl->tpl_vars['content']->value['id'];?>
 hidden">
                    <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['content']->value['zawartosc'],'new_project',$_smarty_tpl->tpl_vars['content']->value['id']);?>

                </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['proj_contenten']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
                <div class="proj_content en <?php echo $_smarty_tpl->tpl_vars['content']->value['id'];?>
 hidden">
                    <?php echo smarty_modifier_replace($_smarty_tpl->tpl_vars['content']->value['zawartosc'],'new_project',$_smarty_tpl->tpl_vars['content']->value['id']);?>

                </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>


            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['projects_table']->value, 'id');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['id']->value) {
?>
                <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p1" id="<?php echo $_smarty_tpl->tpl_vars['id']->value['id'];?>
">
                    
                    <img class="project-child" src="project_pics/<?php echo $_smarty_tpl->tpl_vars['id']->value['id'];?>
.jpg">
                </div>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl);
?>

             
            <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p6" id="new_project">
                <div class="project-child" style="font-size: 12em; background-color:#c0e7d7"> 
                +        
                </div>
            </div>
            
        </div>
    </div>
</div><?php }
}
