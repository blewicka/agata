<?php
$pl = [
        'title' => 'Agata Handzel - graphic artist',
        'portfolio' => 'PORTFOLIO',
        'about' => 'O MNIE',
        'contact' => 'KONTAKT',
        'head_title' => 'Fine Art by Agata Handzel',
        'projects' => 'Galeria',
        'contact_name' => 'Imię i nazwisko',
        'contact_email' => 'Adres e-mail',
        'contact_phone' => 'Numer telefonu',
        'contact_message' => 'Treść wiadomości',
        'contact_submit' => 'Wyślij',
        'info'=>'INFORMACJE',
        'social_media'=>'SOCIAL MEDIA',
];