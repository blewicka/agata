<?php
$en = [
        'title' => 'Agata Handzel - graphic artist',
        'portfolio' => 'PORTFOLIO',
        'about' => 'ABOUT ME',
        'contact' => 'CONTACT',
        'head_title' => 'Fine Art by Agata Handzel',
        'projects' => 'GALLERY',
        'contact_name' => 'Name and surname',
        'contact_email' => 'E-mail address',
        'contact_phone' => 'Phone number',
        'contact_message' => 'Message',
        'contact_submit' => 'Send',
        'info'=>'INFORMATION',
        'social_media'=>'SOCIAL MEDIA',
];