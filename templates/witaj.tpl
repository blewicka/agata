 <div id="center-witaj">
  <div class="row justify-content-center">

      <div class="col">
        <img  src="images/face.png" alt="..." class="img-circle">
      </div>

  </div>

  <div class="row justify-content-center">

      <div class="col col-lg-5 col-md-8 col-xs-12">
        <h2>{$welcome_title}</h2>
        <div id="hello_content">
          {$welcome_content}
        </div>
        
      </div>

  </div>
</div>