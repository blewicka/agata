<div id="center-contact">
    <div class="row justify-content-center">
        <div class="col-lg-5 col-md-8 col-xs-12 ">
                <h2>{$contact}</h2>

                <form action="?action=send-mail" method="post">
                    <div class="form-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="{$contact_name}" required>
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" id="email" name="email" placeholder="{$contact_email}" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="{$contact_phone}" required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="content" rows="5" name="content" placeholder="{$contact_message}" required></textarea>
                    </div>
                    
                    <button type="submit" name="submit" value="submit" class="btn btn-info">{$contact_submit}</button>
                </form>
            </div>
    </div>
</div>