
<div class="row justify-content-center">
       <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">
        <div class="col mini-headers">{$info}</div> 
        <div class="col bottom_text">{$info_content}</div> 
       </div>

       <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">
            <div class="col mini-headers">{$social_media}</div> 
            <a target="_blank" class="smedia-links" href="https://www.etsy.com/shop/ArtworkByAgata?ref=seller-platform-mcnav">
                <img  alt="Etsy" src="icons2/etsy-icon.png" width="40" height="40">
            </a>
           
            <a target="_blank" class="smedia-links" href="https://www.instagram.com/agata.handzel/">
                <img  alt="Instragram" src="icons2/insta-icon.png" width="40" height="40">
            </a>
            
            <a target="_blank" class="smedia-links" href="https://agatahandzel.deviantart.com/">
                <img  alt="DeviantArt" src="icons2/devart-icon.png" width="40" height="40">
            </a>  
             
       </div> 

        <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">
            <div class="col mini-headers">{$about}</div> 
            <div class="col bottom_text">{$about_short_content}</div> 
        </div>
</div>

