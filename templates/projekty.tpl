<div id="projects-page">
    <div class="row justify-content-center">
        <h2>{$projects}</h2>
    </div>
    </br>
    <div class="container">
        <div class="row">
            {* <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p1">
                <div class="project-child" style="background-image: url('images/1.jpg');">         
                </div>
            </div> *}
            {foreach item=id from=$projects_table}
                <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content='{$id.zawartosc|replace:'new_project':$id.id}' id="{$id.id}">
                    {*<div class="project-child" style="background-image: url('project_pics/{$id.id}.jpg');">
                    </div>*}
                    <img class="project-child" src="project_pics/{$id.id}.jpg">
                </div>
            {/foreach}

        </div>
    </div>
</div>