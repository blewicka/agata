{extends file='main.tpl'}
{block name=body}
<div class="row justify-content-center">
 <div class="col-lg-3 col-md-8 col-xs-12 ">

      <form class="form-signin" method="post" action="?page=admin&action=login">
       
        <h2 class="form-signin-heading">Please sign in</h2>
       
        <input type="text" id="login" name="login" class="form-control" placeholder="Login" required autofocus>

        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required>
       
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->
</div>
{/block}