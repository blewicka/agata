{extends file='main.tpl'}


{block name=nav}
    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#projects-page"> {$portfolio} <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#about-me-page">{$about}<span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#contact-page">{$contact} <span class="sr-only">(current)</span></a>
    </li>
    <div class="btn-group nav-item" data-toggle="buttons">
        <label class="btn btn-secondary active">
            <input type="radio" class="lang" data-lang="pl" checked autocomplete="off" id="btn_pl"> PL
        </label>
        <label class="btn btn-secondary">
            <input type="radio" class="lang" data-lang="en" autocomplete="off" id="btn_en"> EN
        </label>
    </div>
{/block}


{block name=css}
    <link rel="stylesheet" type="text/css" href="css/witaj.css">
    <link rel="stylesheet" type="text/css" href="css/projekty.css">
    <link rel="stylesheet" type="text/css" href="css/o-mnie.css">
    <link rel="stylesheet" type="text/css" href="css/kontakt.css">
    <link rel="stylesheet" type="text/css" href="css/media.css">
    <link rel="stylesheet" type="text/css" href="css/overlay.css">
    <link rel="stylesheet" type="text/css" href="css/loading.css">
    <link rel="stylesheet" type="text/css" href="css/admin.css">

    <link rel="stylesheet" type="text/css" href="highlight/default.min.css">
    <link rel="stylesheet" type="text/css" href="katex/katex.min.css">
    <link rel="stylesheet" type="text/css" href="quill/quill.core.css">
    <link rel="stylesheet" type="text/css" href="quill/quill.snow.css">
{/block}

{block name=body}
    {foreach item=trans from=$trans_resultpl}
        <div class="trans_result pl {$trans.nazwa} hidden">
            {$trans.tekst}
        </div>
    {/foreach}

    {foreach item=trans from=$trans_resulten}
        <div class="trans_result en {$trans.nazwa} hidden">
            {$trans.tekst}
        </div>
    {/foreach}


    <div id="overlay" class="overlay">

    <!-- Button to close the overlay navigation -->
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

    <!-- Overlay content -->
    <div class="overlay-content">
        <form action="" onsubmit="return false">
            
            
            <label class="custom-file">
                <input type="file" id="project-pic" class="custom-file-input">
                <span class="custom-file-control"></span>
            </label>
            <br>
            <br>
            <br>
                <div id="project-editor">
                </div>
            
            <br>
            <button class="btn btn-primary" id="project-save">ZAPISZ</button></br></br>
            <button class="btn btn-primary" id="project-delete">USUŃ</button>

        </form>
    </div>
    </div>
    <div class="overlay fade hidden" id="loading">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
    <!-- Overlay content -->
    <div class="page" id="welcome-page">

        {include file="witaj-admin.tpl"}
        
    </div>

    <div class="page" id="projects-page">

        {include file="projekty-admin.tpl"}

    </div>

    <div class="page" id="about-me-page">

        {include file="o-mnie-admin.tpl"}

    </div>

    <div class="page" id="contact-page">

        {include file="kontakt.tpl"}

    </div>

    <div class="page" id="media-page">

        {include file="media-admin.tpl"}

    </div>

    <div id="footer"/>

{/block} 

{block name=js}   
   <script src="js/nawigacja.js"></script>
   <script src="jquery/js/jquery.easing.min.js"></script>
   <script src="highlight/highlight.min.js"></script>
   <script src="katex/katex.min.js"></script>
   <script src="quill/quill.js"></script>
   <script src="quill/quill.min.js"></script>
   <script src="js/witaj-admin.js"></script>
   <script src="js/omnie-admin.js"></script>
   <script src="js/projekty-admin.js"></script> 
   <script src="js/jezyk.js"></script> 
   <script src="js/media-admin.js"></script> 
{/block}
