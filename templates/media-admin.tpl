
<div class="row justify-content-center">
       <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">

            <div class="form-group">
                <textarea class="form-control" value="{$info_content}" rows="3" placeholder="Info"  id="info-content"></textarea>
                
            </div>
       </div>

       <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">
            <div class="col mini-headers">{$social_media}</div> 
            <a target="_blank" class="smedia-links" href="https://www.etsy.com/shop/ArtworkByAgata?ref=seller-platform-mcnav">
                <img  alt="Etsy" src="icons2/etsy-icon.png" width="40" height="40">
            </a>
           
            <a target="_blank" class="smedia-links" href="https://www.instagram.com/agata.handzel/">
                <img  alt="Instragram" src="icons2/insta-icon.png" width="40" height="40">
            </a>
            
            <a target="_blank" class="smedia-links" href="https://agatahandzel.deviantart.com/">
                <img  alt="DeviantArt" src="icons2/devart-icon.png" width="40" height="40">
            </a>  
             
       </div> 

       <div class="col-lg-4 col-md-4 col-xs-12 bottom_text">

            <div class="form-group">
                <textarea class="form-control" value="{$about_short_content}" rows="3" placeholder="O mnie"  id="about_short_content"></textarea>
                
            </div>

            <button class="btn btn-info" id="media-save">ZAPISZ</button>

       </div>
</div>

