 <div id="center-witaj">
  <div class="row justify-content-center">
    <div class="col-lg-6 col-md-8 col-xs-10">  
      <form action="" onsubmit="return false">
            <label class="custom-file">
                <input type="file" id="main-pic" class="custom-file-input">
                <span class="custom-file-control"></span>
            </label><br><br>

            <div class="form-group row">
              <div class="col-10">
                <input class="form-control" value="{$welcome_title}" type="text" placeholder="Tytuł" id="welcome-title">
              </div>
            </div>

            <div class="form-group">
                {* <textarea class="form-control" value="{$welcome_content}" rows="3" placeholder="Tekst powitania"  id="welcome-content"></textarea> *}
                <div id="welcome-editor">
                </div>
            </div>

            <button class="btn btn-info" id="welcome-save">ZAPISZ</button>

        </form>
    </div>
  </div>
</div>