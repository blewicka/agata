<div id="projects-page">
    <div class="row justify-content-center">
        <h2>{$projects}</h2>
    </div>
    </br>
    <div class="container">
        <div class="row" id="projects-container">
            {* <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p1">
                <div class="project-child" style="background-image: url('images/1.jpg');">         
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p2">
                <div class="project-child" style="background-image: url('images/51666.jpg');">         
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p3">
                <div class="project-child" style="background-image: url('images/1.jpg');">         
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p4">
                <div class="project-child" style="background-image: url('images/2.jpg');">         
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p5">
                <div class="project-child" style="background-image: url('images/1.jpg');">         
                </div>
            </div> *}

            {foreach item=content from=$proj_contentpl}
                <div class="proj_content pl {$content.id} hidden">
                    {$content.zawartosc|replace:'new_project':$content.id}
                </div>
            {/foreach}

            {foreach item=content from=$proj_contenten}
                <div class="proj_content en {$content.id} hidden">
                    {$content.zawartosc|replace:'new_project':$content.id}
                </div>
            {/foreach}

            {foreach item=id from=$projects_table}
                <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p1" id="{$id.id}">
                    {*<div class="project-child" style="background-image: url('project_pics/{$id.id}.jpg');">
                    </div>*}
                    <img class="project-child" src="project_pics/{$id.id}.jpg">
                </div>
            {/foreach}
             {*dodawanie nowego projektu --- *}
            <div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p6" id="new_project">
                <div class="project-child" style="font-size: 12em; background-color:#c0e7d7"> 
                +        
                </div>
            </div>
            {*--- dodawanie nowego projektu*}
        </div>
    </div>
</div>