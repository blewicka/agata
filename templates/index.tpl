{extends file='main.tpl'}


{block name=nav}
    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#projects-page"> {$portfolio} <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#about-me-page">{$about}<span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item">
    <a class="nav-link js-scroll-trigger" href="#contact-page">{$contact} <span class="sr-only">(current)</span></a>
    </li>
    <div class="lang-buttons nav-item nav-link">
       <a href="?lang=pl">
                <img class="lang-button" alt="PL" src="icons2/pl-flag.png">
        </a>
           
        <a href="?lang=en">
            <img class="lang-button" alt="EN" src="icons2/en-flag.png" width="22" height="14">
        </a>
    </div>
{/block}


{block name=css}
    <link rel="stylesheet" type="text/css" href="css/witaj.css">
    <link rel="stylesheet" type="text/css" href="css/projekty.css">
    <link rel="stylesheet" type="text/css" href="css/o-mnie.css">
    <link rel="stylesheet" type="text/css" href="css/kontakt.css">
    <link rel="stylesheet" type="text/css" href="css/media.css">
    <link rel="stylesheet" type="text/css" href="css/overlay.css">

    <link rel="stylesheet" type="text/css" href="quill/quill.core.css">
{/block}

{block name=body}

    <div id="overlay" class="overlay">

    <!-- Button to close the overlay navigation -->
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

    <!-- Overlay content -->
    <div class="overlay-arrow left">
        &lsaquo;
    </div>

    <div class="overlay-arrow right">
        &rsaquo;
    </div>
    <div class="overlay-content">

    </div>

    </div> 

    <div class="page" id="welcome-page">

        {include file="witaj.tpl"}

    </div>

    <div class="page" id="projects-page">

        {include file="projekty.tpl"}

    </div>

    <div class="page" id="about-me-page">

        {include file="o-mnie.tpl"}

    </div>

    <div class="page" id="contact-page">

        {include file="kontakt.tpl"}

    </div>

    <div class="page" id="media-page">

        {include file="media.tpl"}

    </div>

    <div id="footer"/>

{/block} 

{block name=js}   
   <script src="js/projekty.js"></script>
   <script src="js/nawigacja.js"></script>
   <script src="jquery/js/jquery.easing.min.js"></script>
{/block}
