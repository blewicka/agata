
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="BLewicka">

    <link rel="icon" type="image/png" href="icons2/favicon.png">
    <link rel="shortcut icon" href="icons2/favicon.png" type="image/png"/>

    
    <title>{$title}</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link href='http://fonts.googleapis.com/css?family=Arima+Madurai&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    {block name=css}
    {/block}

  </head>

      <nav id="mainNav" class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
      
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-brand">{$head_title}</div>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="nav navbar-nav mr-auto">
        
        </ul>
        <ul class="nav navbar-nav ml-auto">
          {block name=nav}
          {/block}
        </ul>
      </div>
    </nav>

    <div class="container-fluid" id="main">

     <div class="starter-template">

  {block name=body}{/block} 
    
     </div>
      
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
      <script src="jquery/js/jquery-3.2.1.min.js"></script>
	  <script src="popper/umd/popper.min.js"></script>
	  <script src="bootstrap/js/bootstrap.min.js"></script>
    

   {block name=js}
   {/block} 
  </body>
</html>
