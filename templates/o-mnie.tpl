<div id="center-o-mnie">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-8 col-xs-10">
      
            <h2> {$about}</h2>
            <div id="o-mnie-content">
                {$about_content}
            </div>
               
        </div>
    </div>
</div>