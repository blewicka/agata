<?php

    class Projects{

        private $lang2;
        private $dtb;

        function __construct($lang,$dbh){
            $this->dtb=$dbh;
            $this->lang2= $lang;
           
        }

        public function all(){
            $all_projects = $this->dtb->prepare("SELECT DISTINCT id FROM projekty");
            $all_projects->execute();
            $project_table = $all_projects->fetchAll();

            return $project_table;
        }

        public function all_content(){
            $all_projects_content = $this->dtb->prepare("SELECT DISTINCT id, zawartosc FROM projekty WHERE jezyk=:jezyk");
            $all_projects_content->bindValue(':jezyk', $this->lang2, PDO::PARAM_STR);
            $all_projects_content->execute();
            $project_table = $all_projects_content->fetchAll();

            return $project_table;
        }

    }

?>

