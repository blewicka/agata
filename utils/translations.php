<?php

    class Translations{

        private $lang2;
        private $dtb;


        function __construct($lang,$dbh){
            $this->dtb=$dbh;
            $this->lang2= $lang;       
        }

        public function all(){
            $text_translations = $this->dtb->prepare("SELECT nazwa, tekst FROM posty WHERE jezyk=:jezyk");
            $text_translations->bindValue(':jezyk', $this->lang2, PDO::PARAM_STR);
            $text_translations->execute();
            $translation_table = $text_translations->fetchAll();

            return $translation_table;
    
        }

    }

?>

