Simple One Page Site
========================

Welcome to the Simple One Page Site - a simple portfolio website for an artist.

What's inside?
--------------

The portofilio uses :

  * PHP 7.X

  * Smarty templates

  * jQuery and jQuery Easing Plugin

  * Bootstrap 4

  * Quill.js text editor
  
  * MariaDB/MySQL database

It allows the user to change content of the website from an admin panel.


To show this page live please open http://agatahandzel.com/

Enjoy!