<?php
require_once 'utils/projects.php';
    class Admin{

        private $smarty2;
        private $dtb;

        function __construct($smarty,$dbh){
            $this->dtb=$dbh;
            $this->smarty2= $smarty;
           
        }

        public function index(){

            if (isset($_SESSION['logged'])){

                $this->smarty2->assign("page",'admin');
                
                $projectspl = new Projects("PL", $this->dtb);
                $projectsen = new Projects("EN", $this->dtb);

                $proj_contentpl=$projectspl->all_content();
                $proj_contenten=$projectsen->all_content();
                $this->smarty2->assign("proj_contentpl", $proj_contentpl);
                $this->smarty2->assign("proj_contenten", $proj_contenten);
                /*var_dump($proj_contentpl);
                var_dump($proj_contenten);*/
                $proj_result=$projectspl->all();
                $this->smarty2->assign("projects_table", $proj_result);

                $translationspl = new Translations('PL', $this->dtb);
                $translationsen = new Translations('EN', $this->dtb);

                $trans_resultpl=$translationspl->all();
                $trans_resulten=$translationsen->all();

                $this->smarty2->assign("trans_resultpl", $trans_resultpl);
                $this->smarty2->assign("trans_resulten", $trans_resulten);
                
                $this->smarty2->display('admin.tpl');
            
            }
            else {
                $this->smarty2->display('login.tpl');
            }
        }
//- przerobić żeby zapisywało to osobno tytył i tręść
        public function welcome_save(){
                        
            $welcome_title = $this->dtb->prepare("REPLACE INTO posty (nazwa, jezyk, tekst) VALUES (:nazwa, :jezyk, :tekst)");
            $welcome_title->bindValue(':nazwa', "welcome_title", PDO::PARAM_STR);
            $welcome_title->bindValue(':jezyk', $_POST['lang'], PDO::PARAM_STR);
            $welcome_title->bindValue(':tekst', $_POST['title'], PDO::PARAM_STR);
            $welcome_title->execute();

            $welcome_content = $this->dtb->prepare("REPLACE INTO posty (nazwa, jezyk, tekst) VALUES (:nazwa, :jezyk, :tekst)");
            $welcome_content->bindValue(':nazwa', "welcome_content", PDO::PARAM_STR);
            $welcome_content->bindValue(':jezyk', $_POST['lang'], PDO::PARAM_STR);
            $welcome_content->bindValue(':tekst', $_POST['content'], PDO::PARAM_STR);
            $welcome_content->execute();

            $uploaddir = 'images/';
            $uploadfile = $uploaddir.'face.png';
            if (isset($_FILES['file'])) {
                if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
                    http_response_code(200);
                } else {
                    http_response_code(500);
                }
            }
          
        }

        public function aboutme_save(){
                       
            $aboutme_content = $this->dtb->prepare("REPLACE INTO posty (nazwa, jezyk, tekst) VALUES (:nazwa, :jezyk, :tekst)");
            $aboutme_content->bindValue(':nazwa', "about_content", PDO::PARAM_STR);
            $aboutme_content->bindValue(':jezyk', $_POST['lang'], PDO::PARAM_STR);
            $aboutme_content->bindValue(':tekst', $_POST['content'], PDO::PARAM_STR);
            $aboutme_content->execute();
          
        }

        public function project_save(){
            //content lang oraz id     
            //var_dump($_FILES);
            $uploaddir = 'project_pics/';
            
            if ($_POST['id'] == 'new_project') {      
                $aboutme_title = $this->dtb->prepare("INSERT INTO projekty (jezyk, zawartosc) VALUES (:jezyk, :zawartosc)");
                $aboutme_title->bindValue(':jezyk', $_POST['lang'], PDO::PARAM_STR);
                $aboutme_title->bindValue(':zawartosc', file_get_contents($_FILES['content']['tmp_name']), PDO::PARAM_STR);
                $aboutme_title->execute();
                $id = $this->dtb->lastInsertId();
                echo $id; 

                $uploadfile = $uploaddir.$id.'.jpg';
                rename("project_pics/new_project", "project_pics/".$id);

                
            }
            else {      
                $aboutme_title = $this->dtb->prepare("REPLACE INTO projekty (id, jezyk, zawartosc) VALUES (:id, :jezyk, :zawartosc)");
                $aboutme_title->bindValue(':id', $_POST['id'], PDO::PARAM_STR);
                $aboutme_title->bindValue(':jezyk', $_POST['lang'], PDO::PARAM_STR);
                $aboutme_title->bindValue(':zawartosc', file_get_contents($_FILES['content']['tmp_name']), PDO::PARAM_STR);
                $aboutme_title->execute();

                $uploadfile = $uploaddir.$_POST['id'].'.jpg';
                
            }
            if (isset($_FILES['file'])) {

                if (move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile)) {
                    http_response_code(200);
                } else {
                    http_response_code(500);
                }
            }

        }

        public function pic_save(){
            
            
            if (file_exists('project_pics/'.$_POST['id'])){
                $uploaddir = 'project_pics/'.$_POST['id'].'/';
                $uploadfile = $uploaddir.md5_file($_FILES['image']['tmp_name']).'.jpg';
                move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile);
            }   
            else {
                mkdir('project_pics/'.$_POST['id'], 0755);
                $uploaddir = 'project_pics/'.$_POST['id'].'/';
                $uploadfile = $uploaddir.md5_file($_FILES['image']['tmp_name']).'.jpg';
                move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile);
            }

                
            echo $uploadfile; 


        }

        public function project_delete(){

            $delete_project = $this->dtb->prepare("DELETE FROM projekty WHERE id=:project_to_delete");
            $delete_project->bindValue(':project_to_delete', $_POST['id'], PDO::PARAM_STR);
            $delete_project->execute();

        }

        public function media_save(){
            
        $info_save = $this->dtb->prepare("REPLACE INTO posty (nazwa, jezyk, tekst) VALUES (:nazwa, :jezyk, :tekst)");
        $info_save->bindValue(':nazwa', "info_content", PDO::PARAM_STR);
        $info_save->bindValue(':jezyk', $_POST['lang'], PDO::PARAM_STR);
        $info_save->bindValue(':tekst', $_POST['info'], PDO::PARAM_STR);
        $info_save->execute();

        $about_short_save = $this->dtb->prepare("REPLACE INTO posty (nazwa, jezyk, tekst) VALUES (:nazwa, :jezyk, :tekst)");
        $about_short_save->bindValue(':nazwa', "about_short_content", PDO::PARAM_STR);
        $about_short_save->bindValue(':jezyk', $_POST['lang'], PDO::PARAM_STR);
        $about_short_save->bindValue(':tekst', $_POST['about'], PDO::PARAM_STR);
        $about_short_save->execute();

        }

        public function login(){

            if ($_POST['2login']=="admin" && $_POST['2password']=='test'){
                $_SESSION['logged']=true;
                $this->index();
            }
            else{
                $this->index();
            }

        }
        
    }

?>
