<?php
require_once 'utils/projects.php';
    
        class Index{

        private $smarty2;
        private $dtb;
        private $lang2;

        function __construct($smarty,$dbh,$lang){
            $this->dtb=$dbh;
            $this->smarty2=$smarty;
            $this->lang2=$lang;
           
        }

        public function index(){
                   
            $projects = new Projects($this->lang2, $this->dtb); // tu zmieniam
            $proj_result=$projects->all_content();
            $this->smarty2->assign("projects_table", $proj_result);
            $this->smarty2->assign("page",'index');
            $this->smarty2->display('index.tpl');
        }

        public function send_mail(){
                $recipient="aisab.lewicka@gmail.com"; //Enter your mail address
                $subject="Contact from Website"; //Subject 
                $sender=$_POST["name"];
                $senderEmail=$_POST["email"];
                $senderPhone=$_POST["phone"];
                $message=$_POST["content"];
                $mailBody="Name: $sender\nEmail Address: $senderEmail\nPhone number: $senderPhone\n\nMessage: $message";
                $headers = "From: aisab.lewicka@gmail.com\r\n";
                mail($recipient, $subject, $mailBody, $headers);
                sleep(1);
        }

    }

?>