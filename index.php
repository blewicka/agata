<?php

        session_start();

        require 'smarty-3.1.30/libs/Smarty.class.php';
        require 'translations/pl.php';
        require 'translations/en.php';
        require 'utils/translations.php';
        require 'controllers/index.php';
        require 'controllers/admin.php';
        require 'controllers/o-mnie.php';
        require 'controllers/kontakt.php';
        require 'controllers/projekty.php';
        

        $smarty = new Smarty;
        $smarty->caching = false;
        $smarty->error_reporting = E_ALL & ~E_NOTICE;
        $dbh = new PDO('mysql:host=localhost;dbname=agata', 'root', 'Cjphonehome1!');

        //jezyki
        $http_lang = substr($_SERVER["HTTP_ACCEPT_LANGUAGE"],0,2);
        
             
      
      


        if (isset($_GET['lang'])){
                switch ($_GET['lang']){
                        case 'en':
                                $lang= $en;
                        break;
                        case 'pl':
                                $lang = $pl;
                        break;
                        default:
                                $lang = $en;
                }
                $lang_name=$_GET['lang'];
                
                
        }
        else {
                switch ($http_lang) {
                        case 'en':
                                $lang= $en;
                        break;
                        case 'pl':
                                $lang = $pl;
                        break;
                        default:
                                 $lang = $en;
                }
                $lang_name=$http_lang;
        }

        //var_dump($lang);
        foreach ($lang as $key => $value){
                $smarty->assign($key,$value);
        }

        $translations = new Translations($lang_name, $dbh, $smarty);
        $trans_result=$translations->all();
        foreach ($trans_result as $key => $value) {
            $smarty->assign($value['nazwa'],$value['tekst']);
        }

        //page
        if (!isset($_GET['page'])){
                $_GET['page']='index';
        }

        switch ($_GET['page']){

        case 'index':
                $index = new Index($smarty,$dbh,$lang_name);
                $index->index();   

                if (isset($_GET['action'])){
                        switch ($_GET['action']){

                                case 'send-mail':
                                $index->send_mail();   
                                break; 

                        }  
                }

        break;





        case 'admin':
                $admin = new Admin($smarty,$dbh);
                if (!isset($_GET['action'])){
                        
                        $admin->index(); 
                }
               else{
                        //action
                        switch ($_GET['action']){

                        case 'welcome-save':
                                $admin->welcome_save();   
                                break;

                        case 'aboutme-save':
                                $admin->aboutme_save(); 
                                break;

                        case 'project-save':
                                $admin->project_save(); 
                                break;
                                
                        case 'project-delete':
                                $admin->project_delete();
                                break;
                        
                        case 'media-save':
                                $admin->media_save();
                                break;
                        
                        case 'login':
                                $admin->login();
                                break;
                        
                        case 'project-image':
                                $admin->pic_save();
                                break;
                        }
                }
        break;

        }    
   


?>
