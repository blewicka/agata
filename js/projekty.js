$(function() {
    var current;
    var open = false;
    $('.overlay-arrow').hide();
    $('#overlay').find('.closebtn').click(function() {
        $('#overlay').css('width', '0%');
        $('.overlay-arrow').hide();
        open=false;
    });

    $('#projects-page').find('.project-parent').click(function() {
        current=this;
        $('#overlay').find('.overlay-content').html($(this).data('content'));
        $('#overlay').css('width', '100%');
        $('.overlay-arrow').show();
        open=true
    });

    $('.overlay-arrow.right').click(function() {
        $(current).next().click();
    });

    $('.overlay-arrow.left').click(function() {
        $(current).prev().click();
    });

    document.onkeydown = checkKey;
    
    function checkKey(e) {
    
        e = e || window.event;
        console.log(e.keyCode);
        if (open) {
            if (e.keyCode == '38') {
                // up arrow
            }
            else if (e.keyCode == '40') {
                // down arrow
            }
            else if (e.keyCode == '37') {
            // left arrow
            $('.overlay-arrow.left').click();
            }
            else if (e.keyCode == '39') {
            // right arrow
            $('.overlay-arrow.right').click();
            } else if (e.keyCode == '27') {
                $('#overlay').find('.closebtn').click();
            }
        }
    }
    
});