var lang = 'pl'

$(function() {
    $('.lang').on('change', function() {
        lang = $(this).data('lang');

        welcomequill.root.innerHTML = $('.trans_result').filter('.'+lang).filter('.welcome_content').html().trim() || '';
        $('#welcome-title').val($('.trans_result').filter('.'+lang).filter('.welcome_title').html().trim() || '');

        aboutmequill.root.innerHTML = $('.trans_result').filter('.'+lang).filter('.about_content').html().trim() || '';

        $('#info-content').val($('.trans_result').filter('.'+lang).filter('.info_content').html().trim() || '');
        $('#about_short_content').val($('.trans_result').filter('.'+lang).filter('.about_short_content').html().trim() || '');
    });

    welcomequill.root.innerHTML = $('.trans_result').filter('.'+lang).filter('.welcome_content').html().trim() || '';
    $('#welcome-title').val($('.trans_result').filter('.'+lang).filter('.welcome_title').html().trim() || '');

    aboutmequill.root.innerHTML = $('.trans_result').filter('.'+lang).filter('.about_content').html().trim() || '';

    $('#info-content').val($('.trans_result').filter('.'+lang).filter('.info_content').html().trim() || '');
    $('#about_short_content').val($('.trans_result').filter('.'+lang).filter('.about_short_content').html().trim() || '');
});