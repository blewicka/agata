$(function() {
    var id;
    /*var imageHandler = function () {
        /*var data = new FormData();
        data.append('image', image);
        data.append('id', id);

        var xhr = new XMLHttpRequest();
        xhr.open('POST', "?page=admin&action=project-image", true);
        //xhr.setRequestHeader('Authorization', 'Client-ID ' + IMGUR_CLIENT_ID);
        xhr.onreadystatechange = function() {
          if (xhr.readyState === 4) {
            var response = JSON.parse(xhr.responseText);
            if (response.status === 200 && response.success) {
              callback(response.data);
            } else {
              var reader = new FileReader();
              reader.onload = function(e) {
                callback(e.target.result);
              };
              reader.readAsDataURL(image);
            }
          }
        }
        xhr.send(data);
        var _this3 = this;
        var fileInput = this.container.querySelector('input.ql-image[type=file]');
        if (fileInput == null) {
        fileInput = document.createElement('input');
        fileInput.setAttribute('type', 'file');
        fileInput.setAttribute('accept', 'image/png, image/gif, image/jpeg, image/bmp, image/x-icon');
        fileInput.classList.add('ql-image');
        fileInput.addEventListener('change', function () {
            if (fileInput.files != null && fileInput.files[0] != null) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var range = _this3.quill.getSelection(true);
                _this3.quill.updateContents(new _quillDelta2.default().retain(range.index).delete(range.length).insert({ image: e.target.result }), _emitter2.default.sources.USER);
                _this3.quill.setSelection(range.index + 1, _emitter2.default.sources.SILENT);
                fileInput.value = "";
            };
            reader.readAsDataURL(fileInput.files[0]);
            }
        });
        this.container.appendChild(fileInput);
        }
        fileInput.click();
}*/function selectLocalImage() {
    const input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.click();

    // Listen upload local image and save to server
    input.onchange = () => {
      const file = input.files[0];

      // file type is only image.
      if (/^image\//.test(file.type)) {
        saveToServer(file);
      } else {
        console.warn('You could only upload images.');
      }
    };
  }

  /**
   * Step2. save to server
   *
   * @param {File} file
   */
  function saveToServer(file) {
    const fd = new FormData();
    fd.append('image', file);
    fd.append('id', id);

    
    const xhr = new XMLHttpRequest();
    $('#loading').addClass('show').removeClass('hidden');
    xhr.open('POST', '?page=admin&action=project-image', true);
    xhr.onload = () => {
      $('#loading').removeClass('show').addClass('hidden');
      if (xhr.status === 200) {
        // this is callback data: url
        const url = xhr.responseText;
        insertToEditor(url.trim());
      }
    };
    xhr.send(fd);
  }

  /**
   * Step3. insert image url to rich editor.
   *
   * @param {string} url
   */
  function insertToEditor(url) {
    // push image url to rich editor.
    const range = quill.getSelection();
    quill.insertEmbed(range.index, 'image', `${url}`);
  }


    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'],    
        ['link', 'image'],    // toggled buttons
        ['blockquote', 'code-block'],
      
        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction
      
        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      
        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],  
        ['clean'],
                                             // remove formatting button
      ];

    var quill = new Quill('#project-editor', {
        modules: {
          formula: true,
          syntax: true,
          toolbar: { 
              container: toolbarOptions,
              handlers: {
                  image: selectLocalImage 
              }
            }
        },
        placeholder: 'Compose an epic...',
        theme: 'snow',
      });
    

    $('#overlay').find('.closebtn').click(function() {
        $('#overlay').css('width', '0%');
    });

    $('#projects-page').find('.project-parent').click(function() {
        // $('#overlay').find('.overlay-content').html($(this).data('content'));
        
        $('#overlay').css('width', '100%');
        id = $(this).attr('id');
        console.log('.proj_content .'+lang+' .'+id);
        if (id!='new_project') {
            if ($('.proj_content').filter('.'+lang).filter('.'+id).length==0) {
                quill.root.innerHTML='';    
            } else {
                quill.root.innerHTML=$('.proj_content').filter('.'+lang).filter('.'+id).html().trim() || '';
            }
            
            $("#project-pic").val("").trigger('change');
        } else {
            quill.root.innerHTML='';
            $("#project-pic").val("").trigger('change');
        }
    });
    
    $('#project-save').click(function() {
        $('#loading').addClass('show').removeClass('hidden');
        var fd = new FormData();
        var file = $('#project-pic')[0].files[0];
		if (typeof file !== 'undefined') {
			fd.append('file', file);
		} else {
			fd.append('file', null);
        }
        var content = quill.root.innerHTML;
        var lang = $('#btn_en').parent().hasClass('active')? 'en' : 'pl'; 
        var content_blob = new Blob([content], {type: 'text/plain'});
        fd.append('content', content_blob);
        fd.append('lang', lang);
        fd.append('id', id);

       
        
        /*$.post('?page=admin&action=project-save', { content: content, lang: lang, id: id }, function(data) {
            console.log(id);
            if (id == 'new_project') {
                id = data;
            }
        });*/

        $.ajax({
            url: "?page=admin&action=project-save",
            type: "POST",
            data: fd,
            enctype: 'multipart/form-data',
            processData: false,  // tell jQuery not to process the data
            contentType: false,   // tell jQuery not to set contentType
            xhr: function() {
                      var myXhr = $.ajaxSettings.xhr();
                      /*if(myXhr.upload){
                          myXhr.upload.addEventListener('progress',progress, false);
                      }*/
                      return myXhr;
          },
          }).done(function( data ) {
            console.log(data)
            if (id == 'new_project') {
                id = data.trim();
                content = content.replace('new_project', id);
                $('<div class="proj_content '+lang+' '+id+' hidden">'+content+'</div>').insertBefore('#new_project');
                $('<div class="col-lg-4 col-md-6 col-xs-12 project-parent" data-content="p6" id="'+id+'">'+
                                                    '<img class="project-child" src="project_pics/'+id+'.jpg">'+
                                               '</div>').insertBefore('#new_project');

                $('#projects-page').find('.project-parent').click(function() {
                    // $('#overlay').find('.overlay-content').html($(this).data('content'));
                    
                    $('#overlay').css('width', '100%');
                    id = $(this).attr('id');
                    console.log('.proj_content .'+lang+' .'+id);
                    quill.root.innerHTML=$('.proj_content').filter('.'+lang).filter('.'+id).html().trim() || '';
                });
            } else {
                if ($('.proj_content').filter('.'+lang).filter('.'+id).length>0) {
                    $('.proj_content').filter('.'+lang).filter('.'+id).html(content);
                } else {
                    $('#projects-container').append('<div class="proj_content '+lang+' '+id+' hidden">'+content+'</div>');
                }
            }

            $('#loading').removeClass('show').addClass('hidden');
            $('#overlay').css('width', '0%');
          }).fail(function() {
            $('#loading').removeClass('show').addClass('hidden');
            $('#overlay').css('width', '0%');
          });
    });

    $('#project-delete').click(function() {
        if (id != 'new_project') {
            $.post("?page=admin&action=project-delete", {id: id}, function() {
                $('#'+id).remove();
                $('#overlay').css('width', '0%');
            });
        }
    });
});
