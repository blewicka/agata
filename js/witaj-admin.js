var welcomequill;
$(function() {
  $('.custom-file-input').on('change',function(){
    var fileName = $(this).val().split('\\');
    $(this).parent().find('.custom-file-control').addClass("selected").html(fileName[fileName.length-1]);
  })
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'],   
        ['link', 'image'],        // toggled buttons
        ['blockquote', 'code-block'],
      
        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction
      
        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      
        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],
      
        ['clean']                                         // remove formatting button
      ];

    welcomequill = new Quill('#welcome-editor', {
        modules: {
          formula: true,
          syntax: true,
          toolbar: toolbarOptions
          //toolbar: '#welcome-toolbar'
        },
        placeholder: 'Compose an epic...',
        theme: 'snow'
      });

    $('#welcome-save').click(function() {
        console.log(welcomequill.root.innerHTML);
        var title = $('#welcome-title').val();
        //var content = $('#welcome-content').val();
        var content = welcomequill.root.innerHTML;
        //var lang = $('#btn_en').parent().hasClass('active')? 'en' : 'pl'; 

        $('#loading').addClass('show').removeClass('hidden');
        var fd = new FormData();
        var file = $('#main-pic')[0].files[0];
        if (typeof file !== 'undefined') {
          fd.append('file', file);
        } else {
          fd.append('file', null);
        }

        fd.append('content', content);
        fd.append('lang', lang);
        //fd.append('id', id);
        fd.append('title', title);
        //$.post('?page=admin&action=welcome-save', { title: title, content: content, lang: lang }, function() {});

        $.ajax({
          url: "?page=admin&action=welcome-save",
          type: "POST",
          data: fd,
          enctype: 'multipart/form-data',
          processData: false,  // tell jQuery not to process the data
          contentType: false,   // tell jQuery not to set contentType
          xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    /*if(myXhr.upload){
                        myXhr.upload.addEventListener('progress',progress, false);
                    }*/
                    return myXhr;
        },
        }).done(function( data ) {
          $('#loading').removeClass('show').addClass('hidden');
        }).fail(function() {
          $('#loading').removeClass('show').addClass('hidden');
        });
    })
});
