var aboutmequill;
$(function() {
    var toolbarOptions = [
        ['bold', 'italic', 'underline', 'strike'],  
        ['link', 'image'],         // toggled buttons
        ['blockquote', 'code-block'],
      
        [{ 'header': 1 }, { 'header': 2 }],               // custom button values
        [{ 'list': 'ordered'}, { 'list': 'bullet' }],
        [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
        [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
        [{ 'direction': 'rtl' }],                         // text direction
      
        [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
        [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
      
        [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
        [{ 'font': [] }],
        [{ 'align': [] }],
      
        ['clean']                                         // remove formatting button
      ];

    aboutmequill = new Quill('#aboutme-editor', {
        modules: {
          formula: true,
          syntax: true,
          toolbar: toolbarOptions
          //toolbar: '#welcome-toolbar'
        },
        placeholder: 'Compose an epic...',
        theme: 'snow'
      });

      
    $('#aboutme-save').click(function() {
        //var title = $('#aboutme-title').val();
        var content = aboutmequill.root.innerHTML;
        var lang = $('#btn_en').parent().hasClass('active')? 'en' : 'pl'; 
        $.post('?page=admin&action=aboutme-save', { /*title: title,*/ content: content, lang: lang }, function() {});
    })
});
